module.exports = {
  apps : [{
    name: 'phishyco-api',
    script: 'npm start',
    watch: true,
    env_production: {
      "NODE_ENV": "production"
    }
  }]
};
