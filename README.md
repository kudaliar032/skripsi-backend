# Phishyco Backend

## Stacks
- Python 3.x (with venv)
- Node.js 12.x (with npm)
- PM2
- MongoDB (native/docker)

## How to Deploy
### Development
- `python3 -m venv`
- `source venv/bin/activate`
- `pip install wheel`
- `pip install -r requirements.txt`
- `npm install`
- `cp .env.example .env` and modified with your environments
- `npm run dev`

### Production
- `python3 -m venv`
- `source venv/bin/activate`
- `pip install wheel`
- `pip install -r requirements.txt`
- `npm install`
- `cp .env.example .env` and modified with your environments
- `npm install -g pm2`
- `pm2 start`

## MongoDB
You can run mongodb with docker, just run `docker-composer up -d`

---
Make with :heart: by. [Aditya Rahman](https://www.linkedin.com/in/adityarahman032/)