const MongoClient = require('mongodb').MongoClient;

const user = encodeURIComponent(process.env.MONGO_USERNAME);
const password = encodeURIComponent(process.env.MONGO_PASSWORD);
const database = encodeURIComponent(process.env.MONGO_DATABASE);
const url = `mongodb://${user}:${password}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/?authMechanism=DEFAULT`;

const client = new MongoClient(url, {useUnifiedTopology: true});
client.connect();
const db = client.db(database);
const col = db.collection('history');

function getHistory(start, length) {
  return new Promise(async (resolve, reject) => {
    try {
      const data = await col.find().skip(start).limit(length).sort({timestamp: -1}).toArray();
      const recordsTotal = await col.countDocuments();

      resolve({recordsTotal, data});
    } catch (e) {
      reject(e);
    }
  })
}

function insertHistory(data) {
  return new Promise((resolve, reject) => {
    col.find({url: data.url}).toArray((err, docs) => {
      if (err) reject(err);
      if (docs < 1) {
        col.insertOne({...data, timestamp: Date.now()}).then(r => {
          resolve(r);
        }).catch(e => {
          reject(e);
        })
      } else {
        col.updateOne({url: data.url}, {$set: {...data, timestamp: Date.now()}}).then(r => {
          resolve(r);
        }).catch(e => {
          reject(e);
        })
      }
    })
  })
}

function historyDetail(url) {
  return new Promise(async (resolve, reject) => {
    try {
      const data = await col.findOne({url});
      resolve(data);
    } catch (e) {
      reject(e);
    }
  })
}

module.exports.insertHistory = insertHistory;
module.exports.getHistory = getHistory;
module.exports.historyDetail = historyDetail;