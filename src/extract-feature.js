const Url = require("url");
const sslChecker = require("ssl-checker");
const axios = require("axios");
const parseXmlString = require("xml2js").parseString;
const whoIsDummy = require("../examples/whois.json");
const base64url = require('base64url');

const features = url => (new Promise(async (resolve, reject) => {
  try {
    const [alexaRankData, pageSpeedData, httpsData, whoIsData] = await Promise.all([
      alexaRank(url),
      pageSpeed(url),
      httpProtocolChecker(url),
      process.env.NODE_ENV === 'production' ? await whoIs(url) : whoIsDummy
    ]);

    resolve({
      features: {
        ip: f1Ip(url),
        simbol_at: f2SymbolAt(url),
        afiks: f3Affix(url),
        usia_domain: f4DomainAge(whoIsData),
        organization: f5Organization(whoIsData),
        privasi: f6Private(whoIsData),
        url_long: f7UrlLong(url),
        url_https: f8UrlHttps(httpsData),
        alexarank: f9AlexaRank(alexaRankData),
        js: f10JavaScript(pageSpeedData),
        page_score: f11PageScore(pageSpeedData)
      },
      screenshot: screenshotBase64(pageSpeedData)
    });
  } catch (e) {
    console.log('Feature extraction error!');
    reject(e);
  }
}));

const baseUrl = url => {
  const myUrl = Url.parse(url);
  return myUrl.hostname;
};

const alexaRank = url => new Promise((async (resolve, reject) => {
  try {
    const {data} = await axios.get(`http://data.alexa.com/data?cli=10&url=${url}`);
    const xml = Buffer.from(data, "utf-8");
    parseXmlString(xml, (err, data) => {
      try {
        resolve(data.ALEXA.SD[0].REACH[0].$.RANK);
      } catch (e) {
        resolve(null);
      }
    });
  } catch (e) {
    console.log('Alexa rank error!');
    reject(e);
  }
}))

const pageSpeed = url => new Promise(async (resolve, reject) => {
  try {
    const {data} = await axios.get(`https://www.googleapis.com/pagespeedonline/v1/runPagespeed?url=${url}&key=${process.env.GOOGLE_API_KEY}&screenshot=true`);
    resolve(data);
  } catch (e) {
    console.log('Google PSI error!')
    reject(e);
  }
});

const whoIs = url => new Promise(async (resolve, reject) => {
  try {
    const {data} = await axios.get(`https://www.whoisxmlapi.com/whoisserver/WhoisService?apiKey=${process.env.WHOIS_API_KEY}&domainName=${url}&outputFormat=json`);
    resolve(data.WhoisRecord);
  } catch (e) {
    console.log('WhoisXML error!')
    reject(e);
  }
});

const httpProtocolChecker = url => new Promise(async (resolve, reject) => {
  try {
    const protocol = Url.parse(url).protocol
    if (protocol !== 'https:') resolve(1)
    const {valid} = await sslChecker(baseUrl(url))
    resolve(valid ? -1 : 0)
  } catch (e) {
    reject(e)
  }
})

const f1Ip = url => {
  return /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(baseUrl(url)) ? 1 : -1;
};

const f2SymbolAt = url => {
  return url.includes("@") ? 1 : -1;
};

const f3Affix = url => {
  const urlWithAffix = url.replace(/(-)|(&)|(\*)|(%20)|(_)|(\.aspx)|(\.php)|(\.html)|(\.ga\/)|(\.website\/)|(\.name\/)|(\.in\/)|(\.space\/)|(\.asia\/)|(\.co\/)|(\.my\/)|(\.web\/)|(\.or\/)|(\.sch\/)|(\.us\/)|(\.xyz\/)|(\.me\/)|(\.ws\/)|(\.tv\/)|(\.mobi\/)|(\.sg\/)|(\.bz\/)|(\.cd\/)|(\.de\/)|(\.hk\/)|(\.gen\/)|(\.firm\/)|(\.jp\/)|(\.kr\/)|(\.la\/)|(\.li\/)|(\.mn\/)|(\.xxx\/)|(\.nz\/)|(\.ph\/)|(\.pk\/)|(\.id\/)|(\.tw\/)|(\.uk\/)|(\.vn\/)|(\.cc\/)|(\.biz\/)|(\.aspx\/)|(\.php\/)|(\.html\/)/g, ";affix;");
  return (urlWithAffix.match(/(;affix;)/g) || []).length;
};

const f4DomainAge = whoIsData => {
  return whoIsData.estimatedDomainAge || 0;
};

const f5Organization = whoIsData => {
  const organization = JSON.stringify(whoIsData.registrant) || "";
  return organization.match(/(organization)|(Organization)/g) ? 1 : -1;
};

const f6Private = whoIsData => {
  const registrant = JSON.stringify(whoIsData.registrant) || "";
  return registrant.match(/(Registration Private)|("")|(REDACTED FOR PRIVACY)/g) ? 1 : -1;
};

const f7UrlLong = url => {
  return url.length;
};

const f8UrlHttps = httpsData => {
  return httpsData
}

const f9AlexaRank = alexaRankData => {
  return alexaRankData ? (alexaRankData > 10000000 ? 0 : -1) : 1
};

const f10JavaScript = pageSpeedData => {
  return pageSpeedData.pageStats.numberJsResources || 0;
};

const f11PageScore = pageSpeedData => {
  return pageSpeedData.score || 0;
};

const screenshotBase64 = pageSpeedData => {
  const {mime_type, data} = pageSpeedData.screenshot;
  return `data:${mime_type};base64,${base64url.toBase64(data)}`
}

const getScreenshot = url => new Promise(async (resolve, reject) => {
  try {
    const {data} = await axios.get(`https://www.googleapis.com/pagespeedonline/v1/runPagespeed?url=${url}&screenshot=true&key=${process.env.GOOGLE_API_KEY}`);
    resolve(data.screenshot);
  } catch (e) {
    reject(e);
  }
});

module.exports = features;
module.exports.getScreenshot = getScreenshot;