import sys
import json
from joblib import load

model = load('./src/random-forest-model.joblib')
min_max = load('./src/min-max-model.joblib')

feature_data = sys.argv[1]


# feature_data = '{"ip":"-1","simbol_at":"-1","afiks":"0","usia_domain":"7219","organization":"1","privasi":"-1","url_long":"24","url_https":"-1","alexarank":"-1","js":"3","page_score":"82"}'


def data_convert(data):
  data_dict = json.loads(data)
  data_list = []

  for i in data_dict:
    data_list.append(int(data_dict[i]))

  return [data_list]


def predict(feature_data):
  return int(model.predict(feature_data)[0])


def proba(feature_data):
  proba = model.predict_proba(feature_data)
  return {'0': float(proba[0][0]), '1': float(proba[0][1])}


if __name__ == '__main__':
  feature_data = data_convert(feature_data)
  normalize_data = min_max.transform(feature_data)

  return_data = {
    'predict': predict(normalize_data),
    'proba': proba(normalize_data)
  }
  json_data = json.dumps(return_data)
  print(json_data)
