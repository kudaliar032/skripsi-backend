require('dotenv').config()

const express = require('express')
const cors = require('cors')
const moment = require('moment')
const {PythonShell} = require('python-shell')
const ExtractFeature = require('./src/extract-feature')
const axios = require('axios')
const db = require('./src/phishyco-db')
const phishingSites = require('./dataset/situs-phishing.json')
const base64url = require('base64url')

const app = express()
const port = process.env.PORT || 4040

moment.locale('id');

if (process.env.NODE_ENV === 'production') {
  app.use(cors({
    origin: 'https://app.phishyco.tech'
  }))
} else {
  app.use(cors())
}

app.get('/', (req, res) => res.send('Phishing Classifier Backend!'))

const handlePredict = (featureData, url, screenshot) => new Promise((resolve, reject) => {
  const pythonOptions = {
    pythonPath: './venv/bin/python',
    args: JSON.stringify(featureData)
  };

  PythonShell.run('./src/model.py', pythonOptions, (err, data) => {
    try {
      if (err) throw err;
      console.log(`---\n${new Date()}\n${url}\n${data[0]}\n${JSON.stringify(featureData)}`);
      db.insertHistory({url, result: JSON.parse(data[0]), features: featureData, screenshot})
      resolve(JSON.parse(data[0]))
    } catch (e) {
      reject(e);
      console.log(e);
    }
  });
});

app.get('/predict', async (req, res) => {
  const url = decodeURIComponent(req.query.url);
  try {
    const {features, screenshot} = await ExtractFeature(url);
    const data = await handlePredict(features, url, screenshot);
    res.send({...data, features, screenshot});
  } catch (e) {
    res.status(400).send(e.message || 'Bad request');
  }
});

app.get('/screenshot', async (req, res) => {
  const url = decodeURIComponent(req.query.url);
  const screenshot = await ExtractFeature.getScreenshot(url);
  var img = Buffer.from(screenshot.data, 'base64');

  res.writeHead(200, {
    'Content-Type': 'image/png',
    'Content-Length': img.length
  });
  res.end(img);
});

app.get('/url-check', (req, res) => {
  const url = decodeURIComponent(req.query.url);
  axios.get(url).then(r => {
    res.send({
      url: url,
      message: r.statusText || 'url valid'
    })
  }).catch(e => {
    res.status(400).send({
      url: url,
      message: e.message || 'url invalid'
    });
  })
});

app.get('/dataset', async (req, res) => {
  try {
    const draw = parseInt(req.query.draw);
    const start = parseInt(req.query.start);
    const length = parseInt(req.query.length);
    const end = start + length;

    const recordsTotal = phishingSites.data.length;
    const recordsFiltered = recordsTotal;
    const data = phishingSites.data.slice(start, end);

    res.send({draw, recordsTotal, recordsFiltered, data});
  } catch (e) {
    res.status(500).send({message: e});
  }
});

app.get('/history', async (req, res) => {
  try {
    const draw = parseInt(req.query.draw) || 1;
    const start = parseInt(req.query.start) || 0;
    const length = parseInt(req.query.length) || 5;

    let {recordsTotal, data} = await db.getHistory(start, length);
    const recordsFiltered = recordsTotal;

    data = data.map(({url, timestamp, result}) => ({
      url, date: moment(timestamp).format('dddd, DD MMM YYYY'), prediction: result.predict
    }))

    res.send({draw, recordsTotal, recordsFiltered, data});
  } catch (e) {
    res.status(500).send({message: e});
  }
});

app.get('/history/:url', async (req, res) => {
  try {
    const {url} = req.params;
    const data = await db.historyDetail(decodeURIComponent(url));
    res.send(data);
  } catch (e) {
    res.status(500).send({message: e});
  }
});

app.listen(port, () => console.log(`Access on port ${port}`));