FROM ubuntu:18.04

ENV APP_DIR /app
ENV GOOGLE_API_KEY null
ENV WHOIS_API_KEY null
ENV ENV dev
ENV PORT 5050

RUN apt update -y && apt upgrade -y
RUN apt install -y python3 python3-pip git curl python3-venv

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt install -y nodejs

WORKDIR $APP_DIR
ADD . $APP_DIR

RUN python3 -m venv venv
RUN venv/bin/pip install wheel
RUN venv/bin/pip install -r requirements.txt

RUN npm install
CMD npm start

EXPOSE $PORT
